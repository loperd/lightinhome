package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/go-ping/ping"
	_ "github.com/go-ping/ping"
	"net/http"
	"time"
)

type errorResponse struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
	Detail  string `json:"detail"`
}

type successResponseData struct {
	PacketLoss int32 `json:"packet_loss"`
	PacketSent int32 `json:"packet_sent"`
}

type successResponse struct {
	Error bool                 `json:"error"`
	Data  *successResponseData `json:"data"`
}

func main() {

	http.HandleFunc("/", handler)

	address := flag.String("address", "", "address to run the web server")
	port := flag.Int("port", 9900, "port to run the web server")
	flag.Parse()

	sprintf := fmt.Sprintf("%s:%d", *address, *port)

	fmt.Printf("Starting HTTP server on %s:%d\n", *address, *port)
	if err := http.ListenAndServe(sprintf, nil); err != nil {
		_ = fmt.Errorf(
			"Can not start http server on address: %s:%d\nError: %s",
			*address, *port, err.Error())
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	if !query.Has("ip") {
		response := &errorResponse{
			Error:   true,
			Message: "Invalid request",
			Detail:  "Doesn't passed ip to ping.",
		}

		writeJsonResponse(w, response)
		return
	}

	defer func() {
		if err := recover(); err != nil {
			response := &errorResponse{
				Error:   true,
				Message: "Invalid request",
				Detail:  "Invalid IP passed.",
			}

			writeJsonResponse(w, response)
		}
	}()

	ipAddress := query.Get("ip")
	err, statistics := pingServerByAddress(ipAddress)

	if err != nil {
		fmt.Println(fmt.Sprintf("Can't ping address \"%s\"", ipAddress))
		return
	}

	sent := int32(statistics.PacketsSent)
	// Packet loss percent
	loss := int32(statistics.PacketLoss)

	fmt.Println(fmt.Sprintf("IP: %s\nSent \"%d\"; Loss \"%d\"\n-----------------", ipAddress, sent, loss))

	data := &successResponseData{
		PacketLoss: loss,
		PacketSent: sent,
	}

	writeJsonResponse(w, &successResponse{Error: 100 == loss, Data: data})
}

func writeJsonResponse(w http.ResponseWriter, r interface{}) {
	headers := map[string]string{
		"Content-Type": "application/javascript",
	}

	for key, value := range headers {
		w.Header().Set(key, value)
	}
	jsonData, _ := json.Marshal(r)
	w.Write(jsonData)
}

func pingServerByAddress(address string) (error, *ping.Statistics) {
	pinger, err := ping.NewPinger(address)
	if err != nil {
		panic(err)
	}

	pinger.Timeout = time.Duration(10) * time.Second
	pinger.Count = 3
	err = pinger.Run()
	if err != nil {
		return err, nil
	}

	return nil, pinger.Statistics()
}
