const serverAddress = "https://pinger.loper.me"
const myIP = "134.249.237.84"

const colors = {
    available: {
        textColor: "ffffff",
        gradientStartColor: "48d420",
        gradientEndColor: "41961b"
    },
    notAvailable: {
        textColor: "f0f0f0",
        gradientStartColor: "9c2424",
        gradientEndColor: "661414"
    }
}

async function isAvailable() {
    const request = new Request(`${serverAddress}/?ip=${myIP}`);
    request.headers = new Headers()
    request.headers.append("Accept", "application/json")

    let response = await request.loadJSON()

    // Scriptable will throw an error to widget in case of internet issues
    return [!response["error"], response["detail"]]
}

function drawHeader(isWifiAvailable, errorText, listWidget, textColor) {
    console.log(errorText);
    const titleText = isWifiAvailable ? "Світло є" : (errorText || "Світла немає")
    const title = listWidget.addText(titleText)
    title.font = Font.boldRoundedSystemFont(20)
    listWidget.addSpacer()

    title.textColor = new Color(textColor)
}

function drawCurrentTime(listWidget, textColor) {
    const dateTime = new Date()
    let df = new DateFormatter()
    df.useShortTimeStyle()

    let description = listWidget.addText(`Оновлено ${df.string(dateTime)}`)
    description.font = Font.mediumSystemFont(13)
    description.textColor = new Color(textColor)
}

function drawWidgetBackgroundGradient(isWifiAvailable, listWidget, activeColors, textColorHEX) {
    const startColor = new Color(activeColors.gradientStartColor)
    const endColor = new Color(activeColors.gradientEndColor)

    const gradient = new LinearGradient()
    gradient.colors = [startColor, endColor]
    gradient.locations = [0.0, 1]

    listWidget.backgroundGradient = gradient
}

async function run() {
    const listWidget = new ListWidget()

    const [isWifiAvailable, errorText] = await isAvailable()
    const activeColors = colors[isWifiAvailable ? "available" : "notAvailable"]
    const {textColor} = activeColors

    drawHeader(isWifiAvailable, errorText, listWidget, textColor)
    drawCurrentTime(listWidget, textColor)

    drawWidgetBackgroundGradient(isWifiAvailable, listWidget, activeColors)

    if (config.runsInApp) {
        listWidget.presentMedium()
    }

    Script.setWidget(listWidget)
    Script.complete()
}

await run()