# Do you have light at home?

### Running in docker container
You need to up web server which will accept all
your requests with your ips and will respond to
you with status and message.

Build image with
```bash
$ make docker-build
```

And up container with HTTP server
```bash
$ make docker-up
```
---
Now we can query this server at
```bash
curl -L http://localhost:3304/?ip=127.0.0.1
```


### Local build for Linux & Mac M1/M2
Required golang 1.18
```bash
$ make build-all
```

### Encode script.js for IOS Widget

Change serverAddress to your server address
and myIp to your home white ip
```js
const serverAddress = "{yourAddress}"
const myIP = "{yourHomeIP}"
```

And encode this script js with all pre-settuped variables 
```bash
$ make build-script_encoder
$ ./bin/script_encoder script.js
```

And take your Light-script.scriptable file to https://scriptable.app

## Credits
- Inspired by https://dou.ua/forums/topic/40716/