FROM golang:1.18-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY *.go ./

RUN mkdir -p /app/bin && go build -o /app/bin/pinger pinger.go

EXPOSE 9900

CMD sh -c "/app/bin/pinger"