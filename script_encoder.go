package main

import (
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type Icon struct {
	Color string `json:"color"`
	Glyph string `json:"glyph"`
}

type Output struct {
	AlwaysRunInApp   bool          `json:"always_run_in_app"`
	Icon             *Icon         `json:"icon"`
	Name             string        `json:"name"`
	Script           string        `json:"script"`
	ShareSheetInputs []interface{} `json:"share_sheet_inputs"`
}

func readFile(configFile string) {
	var err error
	var input = io.ReadCloser(os.Stdin)
	if input, err = os.Open(configFile); err != nil {
		log.Fatalln(err)
	}

	// Read the config file
	jsonBytes, err := ioutil.ReadAll(input)
	input.Close()
	if err != nil {
		log.Fatalln(err)
	}

	inputs := make([]interface{}, 0)

	f := &Output{
		AlwaysRunInApp: false,
		Icon: &Icon{
			Color: "teal",
			Glyph: "plug",
		},
		Name:             "Світло",
		Script:           string(jsonBytes),
		ShareSheetInputs: inputs,
	}

	outputFilename := "Light-script.scriptable"

	b, _ := json.MarshalIndent(f, "", "\t")
	ioutil.WriteFile(outputFilename, b, 0600)

	log.Println("Wrote to file: " + outputFilename)
}

func main() {
	flag.Parse()

	f := flag.Arg(0)
	if f == "" {
		log.Println("File is missing")
		os.Exit(1)
	}

	readFile(f)
}
