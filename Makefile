PROJECT_NAME=lightinhome
IMAGE_TAG=$(PROJECT_NAME):latest

get_filename = $(word 2, $(subst -, ,$1))
get_arch = $(lastword $(subst -, ,$1))
get_os = $(word 3, $(subst -, ,$1))
binfile = bin/$(call get_filename, $1)-$(call get_os, $1)-$(call get_arch, $1)

build-all: build-pinger build-script_encoder

build-pinger: build-pinger-linux-amd64 build-pinger-darwin-arm64
build-pinger-linux-amd64: buildfile-pinger-linux-amd64
build-pinger-darwin-arm64: buildfile-pinger-darwin-arm64

build-script_encoder: build-script_encoder-darwin-arm64 build-script_encoder-darwin-arm64
build-script_encoder-darwin-arm64: buildfile-script_encoder-linux-amd64
build-script_encoder-darwin-arm64: buildfile-script_encoder-darwin-arm64

buildfile-%:
	@env GOOS=$(call get_os, $@) GOARCH=$(call get_arch, $@) go build -o $(call binfile, $@) $(call get_filename, $@).go
	@echo "Successfully build file "$(call binfile, $@)

docker-remove: # Remove old images with tag json-fixer
	@docker images | grep $(PROJECT_NAME) | awk '{ print $$3 }' | xargs docker rmi

docker-build: # Build new image
	@docker build -t $(IMAGE_TAG) .

docker-drop:
	@docker ps -a | grep $(PROJECT_NAME) | awk '{ print $$1 }' | xargs docker rm -f

docker-up:
	@docker run -it -p 3400:9900 --rm -d --name $(PROJECT_NAME) $(IMAGE_TAG)
